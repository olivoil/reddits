duo = ./node_modules/.bin/duo
myth = ./node_modules/.bin/myth

#
# Source wildcards.
#

css = $(shell find lib -name '*.css')
html = $(shell find lib -name '*.html')
js = $(shell find lib -name '*.js')
json = $(shell find lib -name '*.json')

#
# Default.
#

default: build/index.js build/index.css

#
# Targets.
#

build:
	@mkdir -p build

# Build the Javascript source with Duo.
build/index.js: build node_modules $(js) $(html) $(json)
	@mkdir -p build
	@$(duo) lib/boot/index.js > build/index.js

# Build the CSS source with Duo and Myth.
build/index.css: build node_modules $(css)
	@mkdir -p build
	@$(duo) -c lib/boot/index.css | $(myth) > build/index.css

#
# Phony targets.
#

clean:
	rm -rf build components

start:
	@open index.html

.PHONY: clean start
