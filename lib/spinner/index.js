
var autoscale = require('component/autoscale-canvas');
var raf = require('component/raf');

module.exports = Spinner;

function Spinner() {
  var self = this;
  this.percent = 0;

  this.el = document.createElement('canvas');
  this.el.className = 'spinner';

  this.ctx = this.el.getContext('2d');
  this.size(50);
  this.speed(60);
  this.color("RGBA(0, 180, 255, 1)");
  this.stopped = false;

  this.loop();
}

Spinner.prototype.loop = function(){
  if (this.stopped) return;
  raf(this.loop.bind(this));
  this.percent = (this.percent + this._speed / 36) % 100;
  this.draw(this.ctx);
}

Spinner.prototype.start = function(){
  if (!this.stopped) return;
  this.stopped = false;
  this.loop();
}

Spinner.prototype.stop = function(){
  this.stopped = true;
};

Spinner.prototype.size = function(n){
  this.el.width = n;
  this.el.height = n;
  autoscale(this.el);
  return this;
};

Spinner.prototype.speed = function(n) {
  this._speed = n;
  return this;
};

Spinner.prototype.color = function(c){
  this._color = c;
  return this;
};

Spinner.prototype.draw = function(ctx){
  var percent = Math.min(this.percent, 100);
  var ratio = window.devicePixelRatio || 1;
  var size = this.el.width / ratio;
  var half = size / 2;
  var x = half;
  var y = half;
  var rad = half - 1;
  var color = this._color;

  var angle = Math.PI * 2 * (percent / 100);
  ctx.clearRect(0, 0, size, size);

  // outer circle
  var grad = ctx.createLinearGradient(
    half + Math.sin(Math.PI * 1.5 - angle) * half,
    half + Math.cos(Math.PI * 1.5 - angle) * half,
    half + Math.sin(Math.PI * 0.5 - angle) * half,
    half + Math.cos(Math.PI * 0.5 - angle) * half
  );

  // color
  grad.addColorStop(0, 'rgba(255, 255, 255, 0)');
  grad.addColorStop(1, color);

  ctx.fillStyle = grad;
  ctx.beginPath();
  ctx.arc(x, y, rad, angle - Math.PI, angle, false);
  ctx.fill();

  // inner circle
  ctx.fillStyle = 'RGBA(243, 247, 251, 1)';
  ctx.beginPath();
  ctx.arc(x, y, rad - (rad / 3), 0, Math.PI * 2, true);
  ctx.closePath();
  ctx.fill();

  return this;
};
