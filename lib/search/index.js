
var ripple = require('ripplejs/ripple@0.4.0');
var refs = require('ripplejs/refs');
var template = require('./template.html');
var keys = require('ripplejs/shortcuts');
var lifecycle = require('ripplejs/lifecycle');
var events = require('ripplejs/events');
var dispatch = require('ripplejs/dispatch');
var reddit = require('../reddit');

var Search = module.exports = ripple(template)
  .use(refs)
  .use(events)
  .use(dispatch)
  .use(lifecycle())

/**
 * Handle query events.
 */

Search.mounted(function(){
  var self = this;

  this.on('query', function(val){
    self.dispatch('start search', val);

    reddit(val, function(err, posts){
      if (err) return self.emit('error', err);
      self.dispatch('display search results', val, posts);
    });
  });

  this.on('clear', function(){
    self.dispatch('clear search results');
  });

  this.on('error', function(err){
    self.dispatch('handle search error', err);
  });

  setTimeout(function(){
    self.refs.category.value = self.refs.category.value || self.get('category');
    if (self.refs.category.value) self.emit('query', self.refs.category.value);
  }, 250);
});

Search.unmounted(function(){
  this.off('query');
  this.off('clear');
  this.off('error');
});

/**
 * Handle query input.
 */

Search.prototype.search = function(e){
  var self = this;
  var str = this.refs.category.value;

  if (!str) return self.emit('clear');
  if (str.length < 2) return;
  clearTimeout(this.timer);

  this.timer = setTimeout(function(){
    self.timer = null;
    var val = self.refs.category.value;

    if (val === self.get('category')) return;
    self.set('category', val);
    self.emit('query', val);
  }, 350);
};
