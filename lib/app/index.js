
/**
 * Module dependencies.
 */

var ripple = require('ripplejs/ripple@0.4.0');
var template = require('./template.html');
var Search = require('../search');
var List = require('../list');
var lifecycle = require('ripplejs/lifecycle');
var dispatch = require('ripplejs/dispatch');
var Spinner = require('../spinner');

var App = module.exports = ripple(template)
  .use(lifecycle())
  .use(dispatch)
  .compose('search-view', Search)
  .compose('list-view', List)

App.construct(function(){
  this.spinner = new Spinner;
  this.spinner.el.id = 'spinner';
  this.spinner.size(50);
});

App.ready(function(){
  var self = this;

  this.dispatchListener('start search', function(event){
    self.set('savedPosts', self.get('posts'));
    self.set('posts', []);
    self.addSpinner();
  });

  this.dispatchListener('display search results', function(event, category, posts){
    self.removeSpinner();
    self.set('posts', posts || []);
  });

  this.dispatchListener('clear search results', function(event){
    self.set('posts', []);
  });

  this.dispatchListener('handle search error', function(err){
    var posts = self.get('saved posts');
    self.removeSpinner();
    if (posts && posts.length) self.set('posts', posts);
  });
});

App.prototype.addSpinner = function(){
  this.el.appendChild(this.spinner.el);
}

App.prototype.removeSpinner = function(){
  var el = this.spinner.el;
  if (el.parentNode) el.parentNode.removeChild(el);
}
