
var ripple = require('ripplejs/ripple@0.4.0');
var template = require('./template.html');
var lifecycle = require('ripplejs/lifecycle');
var overlay = require('component/overlay');
var filters = require('../list/filters');
var scale = require('../list/scale');
var dragdrop = require('olivoil/dragdrop');
var classes = require('component/classes');

var Item = module.exports = ripple(template)
  .use(filters)
  .use(scale)
  .use(lifecycle())

Item.construct(function(){
  this.overlay = overlay({closable: true});
  this.overlay.on('hide', this.destroy.bind(this));
});

Item.prototype.show = function(){
  this.overlay.show();
  this.appendTo(document.body);
}

Item.directive('draggable', {
  update: function(value, el, view){
    if (!value) return;

    this.draggable = dragdrop(el);

    this.draggable.on('start', function(el, e){
      e.dataTransfer.effectAllowed = 'move';
      e.dataTransfer.setData('http://www.reddit.com' + view.get('permalink'));
    });

    this.draggable.on('end', function(){
      console.log('draggable end', this, arguments);
    });
  },
  unbind: function(el, view){
    this.draggable.off();
  }
});

Item.directive('droppable', {
  update: function(fn, el, view){
    if (!this.dropable){
      this.dropable = dragdrop(el);
    }

    this.dropable.off();
    this.dropable.on('drop', fn.bind(this.view));
  },
  unbind: function(el, view){
    this.dropable.off();
  }
});

Item.prototype.onRedditDrop = function(){
  window.open('http://reddit.com' + this.get('permalink'), '_blank');
}

Item.prototype.onMailDrop = function(){
  window.open('mailto:?Subject=Check out this Reddit post&body=http://reddit.com' + this.get('permalink'), '_blank');
}
