
var template = require('./template.html');
var ripple = require('ripplejs/ripple@0.4.0');
var each = require('ripplejs/each');
var thumb = require('component/thumb');
var lifecycle = require('ripplejs/lifecycle');
var Item = require('../item');
var filters = require('./filters');
var scale = require('./scale');
var events = require('ripplejs/events');

var List = module.exports = ripple(template)
  .use(lifecycle())
  .use(filters)
  .use(scale)
  .use(events)
  .use(each);

/**
 * Show post by index.
 *
 * @param {Number} index
 */

List.prototype.showPost = function(index){
  var posts = this.get('posts') || [];
  var post = posts[index];
  post = post.data || post;
  delete post.$value;

  var item = new Item({
    data: post
  });

  item.show();
}
