
module.exports = function(View){
  View.directive('scale', {
    update: function(value, el, view) {
      var width = parseInt(el.getAttribute('data-width'), 10);
      var height = parseInt(el.getAttribute('data-height'), 10);

      var img = new Image;
      img.src = value;
      img.crossOrigin = "";

      img.onload = function(){
        var canvas = document.createElement('canvas');
        var ctx = canvas.getContext('2d');

        var ratio = width / img.width > height / img.height
          ? width / img.width
          : height / img.height;

        if (ratio < 1) {
          width = Math.ceil(img.width * ratio);
          height = Math.ceil(img.height * ratio);
        }

        canvas.width = width;
        canvas.height = height;
        ctx.drawImage(img, 0, 0, width, height);

        try {
          el.src = canvas.toDataURL('image/jpeg', 1);
        } catch(e){
          // CORS problems ?
          el.src = value;
          el.width = width;
          el.height = height;
        }
      }
    }
  });
}
