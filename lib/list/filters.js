
module.exports = function(View){
  View.filter('defaultThumbnail', function(src){
    return src || "lib/list/images/unknown.png";
  });

  View.filter('comments', function(n){
    switch(n){
      case 0:
        return "no comments";
      case 1:
        return "1 comment";
      default:
        return n + " comments";
    }
  });

  View.filter('ups', function(n){
    switch(n){
      case 0:
        return "no up";
      case 1:
        return "1 up";
      default:
        return n + " ups";
    }
  });

  View.filter('downs', function(n){
    switch(n){
      case 0:
        return "no down";
      case 1:
        return "1 down";
      default:
        return n + " downs";
    }
  });
}
