
var App = require('../app');
require('./polyfill');

var app = new App({
  data: {
    category: 'funny',
    posts: []
  }
});

app.appendTo(document.body);
