
var request = require('visionmedia/superagent');
var isUrl = require('segmentio/is-url');

module.exports = search;

/**
 * Search for post on Reddit.
 *
 * @params {String} category
 * @params {Function} fn
 */

function search(category, fn){
  request
  .get(url(category))
  .set('Accept', 'application/json')
  .end(function(err, res){
    if (err) return fn(err);
    fn(null, parseResponse(res.body));
  });
}

/**
 * Get the reddit search url for `category`.
 *
 * @params {String} category
 * @return {String}
 */

function url(category){
  return "http://www.reddit.com/r/" + category + "/.json";
}

/**
 * Parse response body from Reddit search.
 *
 * @params {Object} body
 * @return {Array}
 */

function parseResponse(body){
  var res = body && body.data && body.data.children || [];

  return res.map(function(post){
    return {
      title: post.data.title,
      author: post.data.author,
      thumbnail: isUrl(post.data.thumbnail) ? post.data.thumbnail : '',
      ups: post.data.ups,
      downs: post.data.downs,
      num_comments: post.data.num_comments,
      permalink: post.data.permalink
    }
  });
}
